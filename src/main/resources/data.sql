INSERT INTO movies (id, title, director, category, duration, release_year, budget) VALUES
  (100, 'Jumanji: The Next Level', 'Jake Kasdan', 0, '124', '2019', '125000000');

INSERT INTO movies (id, title, director, category, duration, release_year, budget) VALUES
  (200, 'Get Out', 'Jordan Peele', 3, '104', '2017', '4500000');

INSERT INTO movies (id, title, director, category, duration, release_year, budget) VALUES
  (300, 'Joker', 'Todd Phillips', 9, '122', '2019', '62500000');