package com.castro.services.movietheater.model;

import com.castro.services.movietheater.enums.MovieCategoryEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "movies")
public class Movie {

    @ApiModelProperty(hidden = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @ApiModelProperty(position = 1, value = "Max length: 100")
    @NotNull
    @Column(name = "title")
    private String title;

    @ApiModelProperty(position = 2)
    @NotNull
    @Column(name = "category")
    private MovieCategoryEnum category;

    @ApiModelProperty(position = 3, value = "Max length: 50")
    @NotNull
    @Column(name = "director")
    private String director;

    @ApiModelProperty(position = 4, value = "Min value: 1")
    @NotNull
    @Column(name = "duration")
    private int duration;

    @ApiModelProperty(position = 5, value = "Min value: 1900")
    @NotNull
    @Column(name = "release_year")
    private int release_year;

    @ApiModelProperty(position = 6, value = "Min value: 1")
    @Column(name = "budget")
    private double budget;
}
