package com.castro.services.movietheater.controller;

import com.castro.services.movietheater.enums.MovieCategoryEnum;
import com.castro.services.movietheater.model.Movie;
import com.castro.services.movietheater.service.MovieService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@ApiResponses({
        @ApiResponse(code = 200, message = "Success"),
        @ApiResponse(code = 400, message = "Bad Request"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 405, message = "Not Allowed"),
        @ApiResponse(code = 415, message = "Unsupported Media Type"),
        @ApiResponse(code = 422, message = "Unprocessable Entity"),
        @ApiResponse(code = 500, message = "Internal Server Error"),
        @ApiResponse(code = 501, message = "Not Implemented")
})
@RequestMapping(
        value = "v1/movie",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE,
        name = "Movie Theater API"
)
public class MovieController {

    private MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping(value = "/search")
    @ApiOperation(
            value = "Retrieve all the movies details."
    )
    public ResponseEntity<List<Movie>> retrieveAllMovies(
            @RequestHeader(value = "Content-Type", defaultValue = MediaType.APPLICATION_JSON_VALUE) final String headers) {

        final List<Movie> moviesList = movieService.findAllMovies();

        return moviesList != null ? ResponseEntity.status(HttpStatus.OK).body(moviesList) :
                ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    @GetMapping(value = "/search/{id}")
    @ApiOperation(
            value = "Retrieve movie details by its ID."
    )
    public ResponseEntity<Movie> searchMovieByID(
            @RequestHeader(value = "Content-Type", defaultValue = MediaType.APPLICATION_JSON_VALUE) final String headers,
            @PathVariable("id") @NotNull final Long id) {

        final Movie movie = movieService.findMovieById(id);

        return movie != null ? ResponseEntity.status(HttpStatus.OK).body(movie) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping(value = "/search/category/{category}")
    @ApiOperation(
            value = "Retrieve all movies from a specific category."
    )
    public ResponseEntity<List<Movie>> searchMovieByCategory(
            @RequestHeader(value = "Content-Type", defaultValue = MediaType.APPLICATION_JSON_VALUE) final String headers,
            @PathVariable("category") @NotNull final MovieCategoryEnum category) {

        final List<Movie> moviesFromCategory = movieService.findMoviesByCategory(category);

        return moviesFromCategory != null ? ResponseEntity.status(HttpStatus.OK).body(moviesFromCategory) :
                ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping(value = "/new")
    @ApiOperation(
            value = "Register a new movie."
    )
    public ResponseEntity<String> registerNewMovie(
            @RequestHeader(value = "Content-Type", defaultValue = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8") final String headers,
            @RequestBody @NotNull @Valid final Movie movie) {

        boolean movieRegistrationStatus = movieService.registerNewMovie(movie);

        return movieRegistrationStatus ? ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    @PutMapping(value = "/update/{id}")
    @ApiOperation(
            value = "Update a specific movie."
    )
    public ResponseEntity<String> updateMovie(
            @RequestHeader(value = "Content-Type", defaultValue = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8") final String headers,
            @PathVariable("id") @NotNull final Long id,
            @RequestBody @NotNull @Valid final Movie movie) {

        return movieService.updateAnExistingMovie(id, movie) ? ResponseEntity.status(HttpStatus.OK).build() :
                ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    @DeleteMapping(value = "/delete/{id}")
    @ApiOperation(
            value = "Delete a movie by ID."
    )
    public ResponseEntity<String> deleteMovieByID(
            @RequestHeader(value = "Content-Type", defaultValue = MediaType.APPLICATION_JSON_VALUE) final String headers,
            @PathVariable("id") @NotNull final Long id) {

        return movieService.deleteMovieById(id) ? ResponseEntity.status(HttpStatus.OK).build() :
                ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}

