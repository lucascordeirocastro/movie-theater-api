package com.castro.services.movietheater.repository;

import com.castro.services.movietheater.enums.MovieCategoryEnum;
import com.castro.services.movietheater.model.Movie;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MovieRepository extends CrudRepository<Movie, Long> {
    List<Movie> findByCategory(MovieCategoryEnum category);
}
