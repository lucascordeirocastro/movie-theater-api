package com.castro.services.movietheater.enums;

public enum MovieCategoryEnum {
    COMEDY,
    ROMANTIC,
    MUSICAL,
    HORROR,
    WAR,
    SCIENCE_FICTION,
    THRILLER,
    WESTERN,
    CRIME,
    DRAMA,
    MYSTERY,
    POLITICAL,
    SATIRE,
    ACTION,
    ADVENTURE,
    FANTASY,
    HISTORICAL,
    BIOGRAPHY,
    PHILOSOPHICAL,
    SOCIAL,
    ANIMATION;
}
