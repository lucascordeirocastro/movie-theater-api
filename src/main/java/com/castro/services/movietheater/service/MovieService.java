package com.castro.services.movietheater.service;

import com.castro.services.movietheater.enums.MovieCategoryEnum;
import com.castro.services.movietheater.model.Movie;
import com.castro.services.movietheater.repository.MovieRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    public List<Movie> findAllMovies() {
        var moviesIterable = movieRepository.findAll();

        var movies = new ArrayList<Movie>();
        moviesIterable.forEach(movies::add);

        return movies.size() >= 1 ? movies : null;
    }

    public Movie findMovieById(Long id) {
        var movieFound = movieRepository.findById(id);

        return movieFound.orElse(null);
    }

    public List<Movie> findMoviesByCategory(MovieCategoryEnum category) {
        var categoryMoviesFound = movieRepository.findByCategory(category);

        return categoryMoviesFound.size() >= 1 ? categoryMoviesFound : null;
    }

    public boolean deleteMovieById(Long id) {
        Optional<Movie> movieToBeRemoved = movieRepository.findById(id);

        if (movieToBeRemoved.isPresent()) {
            movieRepository.deleteById(id);
            return true;
        }

        return false;
    }

    public boolean registerNewMovie(Movie newMovie) {
        log.info(String.valueOf(newMovie));

        if (movieFieldsValidations(newMovie)) {
            movieRepository.save(newMovie);
            return true;
        }

        return false;
    }

    public boolean updateAnExistingMovie(Long id, Movie movie) {
        Optional<Movie> movieFound = movieRepository.findById(id);

        if (movieFieldsValidations(movie)) {
            if (movieFound.isPresent()) {

                movieFound.get().setTitle(movie.getTitle());
                movieFound.get().setDirector(movie.getDirector());
                movieFound.get().setCategory(movie.getCategory());
                movieFound.get().setDuration(movie.getDuration());
                movieFound.get().setRelease_year(movie.getRelease_year());
                movieFound.get().setBudget(movie.getBudget());

                log.info(String.valueOf(movieFound.get()));
                movieRepository.save(movieFound.get());
                return true;
            }
        }
        return false;

    }

    public boolean movieFieldsValidations(Movie movie) {
        return movie.getTitle().length() <= 100 &&
                movie.getDirector().length() <= 50 &&
                movie.getDuration() > 0 &&
                movie.getRelease_year() > 1900 &&
                movie.getBudget() > 0 &&
                EnumUtils.isValidEnum(MovieCategoryEnum.class, movie.getCategory().name());
    }
}
