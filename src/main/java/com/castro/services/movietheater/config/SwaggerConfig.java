package com.castro.services.movietheater.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.castro.services"))
                .paths(PathSelectors.any())
                .build()
                .consumes(getContentType())
                .produces(getContentType())
                .apiInfo(myApiInfo());
    }


    private Set<String> getContentType() {
        HashSet<String> strings = new HashSet<>();
        strings.add(MediaType.APPLICATION_JSON_VALUE);
        return strings;
    }

    private ApiInfo myApiInfo() {
        return new ApiInfo(
                "Movie Theater API",
                "A Movie Theater Management API.",
                "0.1.0.SNAPSHOT",
                "",
                new Contact("Lucas Castro", "", "lucascordeirocastro@gmail.com"),
                "",
                "",
                Collections.emptyList());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
